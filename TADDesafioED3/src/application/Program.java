package application;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import entities.Filme;

public class Program {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		List<Filme> filmes = new ArrayList<>();
		Filme f = new Filme();

		char saida = 's';
		while (saida == 's') {
			System.out.print("Informe o nome do filme: ");
			String nome = sc.nextLine();
			System.out.print("Informe a dura��o do filme: ");
			int duracao = sc.nextInt();
			f = new Filme(nome, duracao);
			f.incluir(filmes, f);
			sc.nextLine();
			System.out.println("Deseja cadastrar um novo filme? s/n");
			saida = sc.nextLine().charAt(0);
		}

		System.out.println("O n�mero de filmes �: " + f.filmesSize(filmes));

		String resposta = (f.consultar(filmes, f)) ? "Existe" : "N�o existe";

		System.out.println("O filme: " + resposta);

		System.out.println("Removendo o �ltimo filme...");
		f.remover(filmes, f);
		System.out.println("O n�mero de filmes �: " + f.filmesSize(filmes));

		System.out.println("Removendo todos os filmes...");
		f.filmesClear(filmes);

		System.out.println("O n�mero de filmes �: " + f.filmesSize(filmes));

		sc.close();
	}

}
