package services;

import java.util.List;

import entities.Filme;

public interface FilmeService {

	public boolean consultar(List<Filme> filmes, Filme filme);

	public void incluir(List<Filme> filmes, Filme filme);

	public void remover(List<Filme> filmes, Filme filme);

	public int filmesSize(List<Filme> filmes);

	public void filmesClear(List<Filme> filmes);
}
