package entities;

import java.util.List;

import services.FilmeService;

public class Filme implements FilmeService{
	
	private String nome;
	private int duracao;
	
	public Filme() {
		
	}

	public Filme(String nome, int duracao) {
		this.nome = nome;
		this.duracao = duracao;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getDuracao() {
		return duracao;
	}

	public void setDuracao(int duracao) {
		this.duracao = duracao;
	}
	
	@Override
	public boolean consultar(List<Filme> filmes, Filme filme) {
		return filmes.contains(filme);
	}

	@Override
	public void incluir(List<Filme> filmes, Filme filme) {
		filmes.add(filme);
		
	}

	@Override
	public void remover(List<Filme> filmes, Filme filme) {
		filmes.remove(filme);
		
	}

	@Override
	public int filmesSize(List<Filme> filmes) {
		return filmes.size();
	}

	@Override
	public void filmesClear(List<Filme> filmes) {
		filmes.clear();
		
	}	
	
	
}
